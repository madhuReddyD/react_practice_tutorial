import SyllabusCard from './SyllabusCard' 
import SyllabusForm from './SyllabusForm' 

function SyllabusItems(props) {

  const syllabusItemsUpdated = (index) => {
    props.syllabusItemsHandler(index)
  }

  let syllabusItems = props.syllabusItems.map((item, index) => {
    if(!item.editMode){
        return(
          <li> <SyllabusCard {...item} syllabusItemsUpdated={syllabusItemsUpdated} index={index}/> </li>  
        )
      }

    else{
      return(
        <li> <SyllabusForm {...item} syllabusItemsUpdated={syllabusItemsUpdated} index={index}></SyllabusForm> </li>
      )
    }
  })

    return(
      <ol>
      {syllabusItems}
      </ol>
  )
}

export default SyllabusItems


// spread operator