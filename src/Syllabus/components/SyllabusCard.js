function SyllabusCard(props) {

    function changeState(){
        props.syllabusItemsUpdated(props.index);
    }
    return (
        <div>
            <p> <b>TITLE:</b>  {props.title} </p>
            <p> <b>DESCRIPTION:</b> {props.description} </p>
            <p> <b>edit Mode:</b> {props.editMode?"true":"false"} </p>
            <button onClick={()=> changeState() }>Edit</button>
            <hr></hr>
        </div>
    )
  }



export default SyllabusCard