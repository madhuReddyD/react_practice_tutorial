import 'C:/Users/Madhu.D/Desktop/REACT Projects/crud_app/src/Syllabus/styles/SyllabusForm.css'

function SyllabusForm(props) {

    function changeState(e){
        e.preventDefault()
        props.syllabusItemsUpdated(props.index);
    }
    
    return(
        <div>
            <form className="form">
                <label>
                    Title: <input type="text" name="title" defaultValue={props.title} />
                </label>
                <br></br>

                <label>
                    Description: <input type="text" name="description" defaultValue={props.description}/>
                </label>
                <br></br>

                <button onClick={changeState} >Save</button>
                <br></br>
                <br></br>

            </form>
            <hr></hr>
            <hr></hr>
        </div>
    )
}

export default SyllabusForm