import SyllabusItems from './components/SyllabusItems'
// import SyllabusForm from './components/SyllabusForm'
import React, {useState} from 'react'

function Syllabus() {

    const [syllabusItems, setSyllabusItems] = useState(
        [
            {
                "title" : "Week1",
                "description" : "Complete Basics by end of Week1",
                "sequenceNumber" : 1,
                "editMode": false
            },
        
            {
                "title" : "Week2",
                "description" : "Complete Basics by end of Week2",
                "sequenceNumber" : 2,
                "editMode": false
            },
        
            {
                "title" : "Week3",
                "description" : "Complete Basics by end of Week3",
                "sequenceNumber" : 3,
                "editMode": false
            },
        
            {
                "title" : "Week4",
                "description" : "End of Course",
                "sequenceNumber" : 4,
                "editMode": true
            },
        ]
    )

    const syllabusItemsHandler = (index) => {
        const syllabusItems1 = [...syllabusItems];
        syllabusItems1[index].editMode = !syllabusItems[index].editMode 
        setSyllabusItems(syllabusItems1)
    }

    return (
        <div>
            {/* <SyllabusForm></SyllabusForm> */}
            <SyllabusItems syllabusItemsHandler={syllabusItemsHandler} syllabusItems={syllabusItems} />
        </div>
    )
}

export default Syllabus