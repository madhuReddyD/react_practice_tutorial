import './App.css';
import Syllabus from './Syllabus/index';

function App() {
  return (
    <div className="App">
      <header className="App-custom-header">
        <h1> Syllabus Card</h1>
      </header>
      <Syllabus></Syllabus>
    </div>
  );
}

export default App;
